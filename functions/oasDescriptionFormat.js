module.exports = (targetVal) => {
if (!targetVal) {
    return;
  }
  var regex = new RegExp("^[A-Z].*\\.$");
  var nameNoMarkdown = targetVal.replace("|","").replace("#","").replace("*","").replace(":","").replace(/(\r\n|\n|\r)/gm,"").replace(/\s+/g,"").trim();
  if (!regex.test(nameNoMarkdown)){
  	 return [{message: 'Description should start with upper case and end with a dot. Can include markdown. Invalid value: '+ targetVal}];
  }
};