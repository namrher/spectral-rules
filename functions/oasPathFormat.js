module.exports = (targetVal) => {

  const INVALID_PATH = " (invalid path). ";
  const regexKebabCase = new RegExp("^[a-z]+(?:-[a-z0-9]+)*$");
  const regexStartWithCurlyBrace = new RegExp("^{");

  if (!targetVal) {
    return;
  }
  var isPathParam = true, isFirstPathValue = true;
  var errorMessage = '';
  var lastPathParam = '';
  targetVal.split('/').forEach(pathValue => {
    if (pathValue != '') {
    if (regexStartWithCurlyBrace.test(pathValue)) {
      if (isPathParam) {
        errorMessage = (isFirstPathValue ? ("Don't start a path with a path param: " + pathValue) : ("Don't use consecutive path params: " + lastPathParam + "/" + pathValue)) + ". ";
      }
      lastPathParam = pathValue;
      isPathParam = true;
    }else {
      isPathParam = false;
    }
    isFirstPathValue = false;
    if (pathValue != '' && !regexKebabCase.test(pathValue.replace("{", "").replace("}",""))) {
      errorMessage = errorMessage + "Include only kebab-case in path: " + pathValue + ". ";
    }
    }
  }); 
  if (errorMessage != '') {
    return [{message: targetVal + INVALID_PATH +  errorMessage}];
  }
  
};