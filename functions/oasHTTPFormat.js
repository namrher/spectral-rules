

module.exports = (targetVal, opts) => {

  const httpFormat = new RegExp("https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)");

if (!targetVal) {
    return;
  }
  

  var regex;
  switch (opts.type) {
    case "camel": regex = camel; break;
    case "pascal": regex = pascal; break;
    case "kebab": regex = kebab; break;
    case "cobol": regex = cobol; break;
    default: return "Define a valid case: camel, pascal, kebab, cobol."
  }

  if (!regex.test(targetVal)){
  	 return [{message: (targetVal + " should use " + opts.type +" case notation.")}];
  }

};