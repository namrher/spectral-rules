

module.exports = (targetVal, opts) => {

  const camel = new RegExp("(^[a-z]+)|(^[a-z]+([A-Z][a-z0-9]+)+)");
  const pascal =new RegExp("^([A-Z])+[A-Za-z]");
  const kebab = new RegExp("^[a-z]+(?:-[a-z]+)*$");
  const cobol = new RegExp("/^[A-Z]+(?:-[A-Z]+)*$/");

  if (!targetVal) {
      return;
    }
  
  var regex;
  switch (opts.type) {
    case "camel": regex = camel; break;
    case "pascal": regex = pascal; break;
    case "kebab": regex = kebab; break;
    case "cobol": regex = cobol; break;
    default: return "Define a valid case: camel, pascal, kebab, cobol."
  }
  
  if (!regex.test(targetVal)){
  	 return [{message: (targetVal + " should use " + opts.type +" case notation.")}];
  }

};