module.exports = (targetVal) => {
if (!targetVal) {
    return;
  }
  const responses = Object.keys(targetVal);
  if (responses.filter(response => Number(response) >= 200 && Number(response) < 300).length === 0) {
    return [{message: 'Operations must define at least one 2xx response.'}];
  }
};