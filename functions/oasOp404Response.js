module.exports = (targetVal) => {
  if (!targetVal) {
      return;
    }
    var errorMessages= [];
    for (var path in targetVal) {
      if (path.includes('{')) {
        for (var verb in targetVal[path]) {
          var is404Included = false;
          for (var response in targetVal[path][verb].responses) {
            if (Number(response) == 404) {
              is404Included = true;
              break;
            } 
          }
          if (!is404Included) {
            errorMessages.push({message: (verb + ' ' + path + ': Include 404 error if path param is included in path.')});
          }
        }
      }
    }
    if (errorMessages.length > 0) {
      return errorMessages;
    }
  };
