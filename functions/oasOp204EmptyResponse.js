module.exports = (targetVal) => {
  if (!targetVal) {
      return;
    }
    for (var response in targetVal) {
      if (Number(response) == 204) {
        for (var atrib in targetVal[response]){
          if (atrib == 'content') {
            return [{message: '204 response must be empty.'}];
          }
        }
      } 
    }
  };
  