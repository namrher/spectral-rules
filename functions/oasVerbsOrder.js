module.exports = (targetVal) => {

  var verbs = ['options', 'head', 'get', 'post', 'put', 'patch', 'delete', 'trace'];

  if (!targetVal) {
      return;
    }
    var errorMessages= [];
    for (var path in targetVal) {
        verbIndex = 0;
        for (var verb in targetVal[path]) {
          newIndex = verbs.indexOf(verb);
          if (newIndex < verbIndex) {
            errorMessages.push({message: (verb + ' ' + path + ': Verbs in a path must be ordered in this order: ' + verbs)});
            break;
          } else {
            verbIndex = newIndex;
          }
      }
    }
    if (errorMessages.length > 0) {
      return errorMessages;
    }
  };
