# Spectral-rules test

## Test yout rules

You can test your rules with:

*Openapi 3*
~~~~
sudo spectral lint examples/example_openapi_.yaml -r ./oas3-rules.json
~~~~

*Swagger 2*
~~~~
sudo spectral lint examples/example_swagger_2.yaml -r ./oas2-rules.json
~~~~
